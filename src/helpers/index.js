

export const formatearFecha = (fechaParam) => {
  const nuevaFecha = new Date(fechaParam);
  const options = {
    weekday: 'long',
    year: 'numeric',
    month: 'long',
    day: 'numeric',
  };

  return nuevaFecha.toLocaleDateString('es-ES', options);
};
