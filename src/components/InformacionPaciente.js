import React from 'react';
import {Text, SafeAreaView, Pressable, View, StyleSheet} from 'react-native';
import { formatearFecha } from '../helpers';

export const InformacionPaciente = ({paciente, setModalPaciente, setPaciente}) => {

  return (
    <SafeAreaView
      style={styles.contenedor}
    >
      <Text style={styles.titulo}>Informacion Paciente</Text>

      <View>
        <Pressable
          style={styles.btnCerrar}
          onPress={() => {
            setModalPaciente(false);
            setPaciente({});
          }}
        >
          <Text  style={styles.btnCerrarTxt}>Cerrar</Text>
        </Pressable>
      </View>

      <View
        style={styles.contenido}
      >

        <View style={styles.campo}>
          <Text style={styles.label}>Nombre</Text>
          <Text style={styles.valor}>{paciente.paciente}</Text>
        </View>

        <View style={styles.campo}>
          <Text style={styles.label}>Propietario</Text>
          <Text style={styles.valor}>{paciente.propietario}</Text>
        </View>

        <View style={styles.campo}>
          <Text style={styles.label}>Email</Text>
          <Text style={styles.valor}>{paciente.email}</Text>
        </View>

        <View style={styles.campo}>
          <Text style={styles.label}>Email</Text>
          <Text style={styles.valor}>{paciente.telefono}</Text>
        </View>

        <View style={styles.campo}>
          <Text style={styles.label}>Alta</Text>
          <Text style={styles.valor}>{formatearFecha(paciente.fecha)}</Text>
        </View>

        <View style={styles.campo}>
          <Text style={styles.label}>Sintomas</Text>
          <Text style={styles.valor}>{paciente.sintomas}</Text>
        </View>
      </View>

    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  contenedor: {
    backgroundColor: '#F59E0B',
    flex: 1,
  },
  titulo: {
    textAlign: 'center',
    fontSize: 30,
    color: '#FFF',
    fontWeight: '600',
  },
  btnCerrar: {
    marginVertical: 30,
    backgroundColor: '#c84b1d',
    marginHorizontal: 30,
    padding: 15,
    borderRadius: 10,
  },
  btnCerrarTxt: {
    color: 'white',
    textAlign: 'center',
    fontWeight: '900',
    fontSize: 16,
    textTransform: 'uppercase',
  },
  contenido: {
    backgroundColor: '#FFF',
    marginHorizontal: 30,
    borderRadius: 10,
    padding: 20,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.29,
    shadowRadius: 4.65,

    elevation: 7,
  },
  campo: {
    marginBottom: 10,
  },
  label: {
    textTransform: 'uppercase',
    color: '#374151',
    fontWeight: '600',
    fontSize: 12,
  },
  valor: {
    fontWeight: '700',
    fontSize: 20,
    color: '#334155',
  }
});
