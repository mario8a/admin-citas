import React, {useState} from 'react';
import {
  Text,
  SafeAreaView,
  StyleSheet,
  Pressable,
  FlatList,
  Alert,
  Modal
} from 'react-native';

import { Formluario } from './src/components/Formulario';
import { Paciente } from './src/components/Paciente';
import { InformacionPaciente } from './src/components/InformacionPaciente';

const App = () => {

  const [modalVisible, setModalVisible] = useState(false);
  const [pacientes, setPacientes] = useState([]);
  const [paciente, setPaciente] = useState({});
  const [modalPaciente, setModalPaciente] = useState(false);

  const pacienteEditar = id => {
    const pacienteEditar = pacientes.filter(pacient => pacient.id === id);
    setPaciente(pacienteEditar[0]);
  };

  const eliminarPaciente = id => {
    Alert.alert(
      '¿Desear eliminar este paciente?',
      'Un paciente eliminado no se puede recuperar',
      [
        { text: 'Cancelar', style: 'cancel' },
        { text: 'Eliminar', onPress: () => {
          const pacientesActualizados = pacientes.filter(pacientesState => pacientesState.id !== id);
          setPacientes(pacientesActualizados);
        }}
      ]
    )
  };

  const cerrarModal = () => {
    setModalVisible(false);
  }

  return (
    <SafeAreaView style={styles.container}>
      <Text style={styles.titulo}>
        Administrador de Citas Veterinaria
      </Text>

      <Pressable
        onPress={() => setModalVisible(!modalVisible)}
        style={styles.btnNuevaCita}
      >
        <Text style={styles.btnTextoNuevaCita}>Nueva cita</Text>
      </Pressable>

      {
        pacientes.length === 0 ?
          <Text style={styles.noPacientes}>No hay pacientes</Text>
          :
          <FlatList
            style={styles.listado}
            data={pacientes}
            keyExtractor={(item) => item.id}
            renderItem={({item}) => {
              return (
                <Paciente
                  item={item}
                  setModalVisible={setModalVisible}
                  setPaciente={setPaciente}
                  pacienteEditar={pacienteEditar}
                  eliminarPaciente={eliminarPaciente}
                  setModalPaciente={setModalPaciente}
                />
              );
            }}
          />
      }

      {
        modalVisible && (
          <Formluario
          modalVisible={modalVisible}
            cerrarModal={cerrarModal}
            setPacientes={setPacientes}
            pacientes={pacientes}
            paciente={paciente}
            setPaciente={setPaciente}
          />
        )
      }


      <Modal
        visible={modalPaciente}
        animationType="fade"
      >
        <InformacionPaciente
          paciente={paciente}
          setModalPaciente={setModalPaciente}
          setPaciente={setPaciente}
        />
      </Modal>

    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#f4f3f6',
    flex: 1,
  },
  titulo: {
    textAlign: 'center',
    fontSize: 30,
    color: '#374151',
    fontWeight: '600',
  },
  btnNuevaCita: {
    backgroundColor: '#6a0493',
    padding: 15,
    marginTop: 30,
    marginHorizontal: 20,
    borderRadius: 10,
  },
  btnTextoNuevaCita: {
    textAlign: 'center',
    color: '#fff',
    fontSize: 15,
    fontWeight: '900',
    textTransform: 'uppercase',
  },
  noPacientes: {
    marginTop: 40,
    textAlign: 'center',
    fontSize: 24,
    fontWeight: '600',
  },
  listado: {
    marginTop: 30,
    marginHorizontal: 30,
  }
});

export default App;
